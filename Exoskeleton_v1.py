# -*-coding: utf-8-*-
#!/usr/bin/env python


#----------------------------------------------------------------------------------
#                                 Exoskeleton v1.0
#   -Driver Encoder:
#                   - Initialization
#                   -  Prueba Bitbucket
#                       -Pichulin se la come
#----------------------------------------------------------------------------------

__author__ = 'Giancarlo Villena'
__version__ = '1.0.0'
__date__ = 'Mar 18 2017'


#Basic imports
from ctypes import *
import xlsxwriter
import random
from time import sleep

#Phidget specific imports
from Phidgets.PhidgetException import PhidgetErrorCodes, PhidgetException
from Phidgets.Events.Events import AttachEventArgs, DetachEventArgs, ErrorEventArgs, EncoderPositionChangeEventArgs, InputChangeEventArgs, OutputChangeEventArgs, SensorChangeEventArgs
from Phidgets.Devices.Encoder import Encoder
from Phidgets.Devices.InterfaceKit import InterfaceKit

#Create an excel file
hg
# Creacion del fichero
workbook = xlsxwriter.Workbook('MuscleTest.xlsx') # file name
worksheet1 = workbook.add_worksheet('Test Number 1') # modify each test
bold = workbook.add_format({'bold': 1})
currency_format = workbook.add_format({'num_format': '$#,##0'})
headings = [u'Posición', u'Presión']

#Configuracion de la hoja
caption = 'Data del Encoder'
# Set the columns widths.
worksheet1.set_column('B:G', 16)
# Write the caption.
worksheet1.write('B1', caption)
# Write the headings.
worksheet1.write_row('B3', headings, bold)
# Colums and rows
column_posi=1
column_pres=2
row=3


#Create devices objects
try:
    encoder = Encoder()
    interfaceKit=InterfaceKit()
except RuntimeError as e:
    print("Runtime Exception: %s" % e.details)
    print("Exiting....")
    exit(1)

#Information Display Function
def displayDeviceInfo():
    print("|------------|----------------------------------|--------------|------------|")
    print("|- Attached -|-              Type              -|- Serial No. -|-  Version -|")
    print("|------------|----------------------------------|--------------|------------|")
    print("|- %8s -|- %30s -|- %10d -|- %8d -|" % (encoder.isAttached(), encoder.getDeviceName(), encoder.getSerialNum(), encoder.getDeviceVersion()))
    print("|- %8s -|- %30s -|- %10d -|- %8d -|" % (interfaceKit.isAttached(), interfaceKit.getDeviceName(), interfaceKit.getSerialNum(), interfaceKit.getDeviceVersion()))
    print("|------------|----------------------------------|--------------|------------|")
    print("Number of Digital Inputs: %i" % (interfaceKit.getInputCount()))
    print("Number of Digital Outputs: %i" % (interfaceKit.getOutputCount()))
    print("Number of Sensor Inputs: %i" % (interfaceKit.getSensorCount()))


#Event Handler Callback Functions
def encoderAttached(e):
    attached = e.device
    print("Encoder %i Attached!" % (attached.getSerialNum()))

def encoderDetached(e):
    detached = e.device
    print("Encoder %i Detached!" % (detached.getSerialNum()))

def encoderError(e):
    try:
        source = e.device
        print("Encoder %i: Phidget Error %i: %s" % (source.getSerialNum(), e.eCode, e.description))
    except PhidgetException as e:
        print("Phidget Exception %i: %s" % (e.code, e.details))

def encoderInputChange(e):
    source = e.device
    print("Encoder %i: Input %i: %s" % (source.getSerialNum(), e.index, e.state))

def encoderPositionChange(e):
    source = e.device
    print("Encoder %i: Encoder %i -- Change: %i -- Time: %i -- Position: %i" % (source.getSerialNum(), e.index, e.positionChange, e.time, encoder.getPosition(e.index)))

def interfaceKitAttached(e):
    attached = e.device
    print("InterfaceKit %i Attached!" % (attached.getSerialNum()))

def interfaceKitDetached(e):
    detached = e.device
    print("InterfaceKit %i Detached!" % (detached.getSerialNum()))

def interfaceKitError(e):
    try:
        source = e.device
        print("InterfaceKit %i: Phidget Error %i: %s" % (source.getSerialNum(), e.eCode, e.description))
    except PhidgetException as e:
        print("Phidget Exception %i: %s" % (e.code, e.details))

def interfaceKitInputChanged(e):
    source = e.device
    print("InterfaceKit %i: Input %i: %s" % (source.getSerialNum(), e.index, e.state))

def interfaceKitSensorChanged(e):
    source = e.device
    print("InterfaceKit %i: Sensor %i: %i" % (source.getSerialNum(), e.index, e.value))

def interfaceKitOutputChanged(e):
    source = e.device
    print("InterfaceKit %i: Output %i: %s" % (source.getSerialNum(), e.index, e.state))

print("Opening phidget object....")

try:
    encoder.openPhidget()
except PhidgetException as e:
    print("Phidget Error %i: %s" % (e.code, e.details))
    exit(1)

print("Waiting for attach....")

try:
    encoder.waitForAttach(10000)
except PhidgetException as e:
    print("Phidget Error %i: %s" % (e.code, e.details))
    try:
        encoder.closePhidget()
    except PhidgetException as e:
        print("Phidget Error %i: %s" % (e.code, e.details))
        exit(1)
    exit(1)
else:
    displayDeviceInfo()

print("Setting the data rate for each sensor index to 4ms....")

for i in range(interfaceKit.getSensorCount()):
    try:
        interfaceKit.setDataRate(i, 4)
    except PhidgetException as e:
        print("Phidget Exception %i: %s" % (e.code, e.details))



print "Codigo"

# Enable Encoder
encoder.setEnabled(3,True)
encoder.getEnabled(3)


itfk_anterior = interfaceKit.getSensorRawValue(0)
encoder_anterior = encoder.getPosition(3)

estado = encoder.getInputState(0)
print ("Input State :"), estado

while not (encoder.getInputState(0)):
    itfk_actual = interfaceKit.getSensorRawValue(0)
    encoder_actual = encoder.getPosition(3)

    if (itfk_anterior != itfk_actual) or (encoder_anterior != encoder_actual):

        itfk_actual = interfaceKit.getSensorRawValue(0)

        volt = float(itfk_actual * 0.00122)
        presion = ((volt / 5.0) - 0.04) * 777.726

        #print "Valor Raw del voltaje leido en digital", itfk_actual
        print ("Voltaje leido en voltios %.2f") % volt
        #print ("Valor de la presion Raw %.2f") % presion
        print ("presion = %.2f") % presion

        itfk_anterior = itfk_actual
        encoder_anterior = encoder_actual
        angle = float(encoder_anterior*(360*1.0/4800)*1.0)
        print ("angulo =" , float(angle))

        if angle and presion:
            # Add a table to the worksheet.
            worksheet1.write(row, column_posi, str(angle))
            worksheet1.write(row, column_pres, str(presion))
            row += 1
        else:
            print("No es valido el caracter ingresado")

workbook.close()
print("Closing...")
try:
    encoder.closePhidget()
    interfaceKit.closePhidget()
except PhidgetException as e:
    print("Phidget Error %i: %s" % (e.code, e.details))
    print("Exiting....")
    exit(1)

print("Done.")
exit(0)