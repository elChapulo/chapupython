# -*-coding: utf-8-*-
#!/usr/bin/env python


#----------------------------------------------------------------------------------
#                                 Exoskeleton v1.0
#   -Driver Encoder:
#                   - Initialization
#----------------------------------------------------------------------------------

__author__ = 'Giancarlo Villena'
__version__ = '1.3'
__date__ = 'Feb 22 2017'


#Basic imports
from ctypes import *
import xlsxwriter
import random
import time
import numpy as np
import matplotlib.pyplot as plt

#Phidget specific imports
from Phidgets.PhidgetException import PhidgetErrorCodes, PhidgetException
from Phidgets.Events.Events import AttachEventArgs, DetachEventArgs, ErrorEventArgs, EncoderPositionChangeEventArgs, InputChangeEventArgs, OutputChangeEventArgs, SensorChangeEventArgs
from Phidgets.Devices.Encoder import Encoder
from Phidgets.Devices.InterfaceKit import InterfaceKit

#Create an excel file

# Creacion del fichero 1
workbook = xlsxwriter.Workbook('MuscleTest.xlsx') # file name
worksheet1 = workbook.add_worksheet('Test Number 1') # Angle
bold1 = workbook.add_format({'bold': 1})
currency_format1 = workbook.add_format({'num_format': '$#,##0'})
headings1 = [u'Posición (deg)', u'Tiempo (s)']

#Configuracion de la hoja
caption1 = 'Data del Encoder'
# Set the columns widths.
worksheet1.set_column('B:G', 16)
# Write the caption.
worksheet1.write('B1', caption1)
# Write the headings.
worksheet1.write_row('B3', headings1, bold1)
# Colums and rows
column_pos1 = 1
column_pos2 = 2
row=3


# Creacion del fichero 2
#workbook2 = xlsxwriter.Workbook('PressureSensor.xlsx') # file name
worksheet2 = workbook.add_worksheet('Test Number 2') # Presion
bold2 = workbook.add_format({'bold': 1})
currency_format2 = workbook.add_format({'num_format': '$#,##0'})
headings2 = [u'Presion (MPa)', u'Tiempo (s)']

#Configuracion de la hoja
caption2 = 'Data del Pressure Sensor'
# Set the columns widths.
worksheet2.set_column('B:G', 16)
# Write the caption.
worksheet2.write('B1', caption2)
# Write the headings.
worksheet2.write_row('B3', headings2, bold2)



#Create devices objects
try:
    encoder = Encoder()
    interfaceKit=InterfaceKit()
except RuntimeError as e:
    print("Runtime Exception: %s" % e.details)
    print("Exiting....")
    exit(1)

#Information Display Function
def displayDeviceInfo():
    print("|------------|----------------------------------|--------------|------------|")
    print("|- Attached -|-              Type              -|- Serial No. -|-  Version -|")
    print("|------------|----------------------------------|--------------|------------|")
    print("|- %8s -|- %30s -|- %10d -|- %8d -|" % (encoder.isAttached(), encoder.getDeviceName(), encoder.getSerialNum(), encoder.getDeviceVersion()))
    print("|- %8s -|- %30s -|- %10d -|- %8d -|" % (interfaceKit.isAttached(), interfaceKit.getDeviceName(), interfaceKit.getSerialNum(), interfaceKit.getDeviceVersion()))
    print("|------------|----------------------------------|--------------|------------|")

#Event Handler Callback Functions
def encoderAttached(e):
    attached = e.device
    print("Encoder %i Attached!" % (attached.getSerialNum()))

def encoderDetached(e):
    detached = e.device
    print("Encoder %i Detached!" % (detached.getSerialNum()))

def encoderError(e):
    try:
        source = e.device
        print("Encoder %i: Phidget Error %i: %s" % (source.getSerialNum(), e.eCode, e.description))
    except PhidgetException as e:
        print("Phidget Exception %i: %s" % (e.code, e.details))

def encoderInputChange(e):
    source = e.device
    print("Encoder %i: Input %i: %s" % (source.getSerialNum(), e.index, e.state))

def encoderPositionChange(e):
    source = e.device
    print("Encoder %i: Encoder %i -- Change: %i -- Time: %i -- Position: %i" % (source.getSerialNum(), e.index, e.positionChange, e.time, encoder.getPosition(e.index)))

def interfaceKitAttached(e):
    attached = e.device
    print("InterfaceKit %i Attached!" % (attached.getSerialNum()))

def interfaceKitDetached(e):
    detached = e.device
    print("InterfaceKit %i Detached!" % (detached.getSerialNum()))

def interfaceKitError(e):
    try:
        source = e.device
        print("InterfaceKit %i: Phidget Error %i: %s" % (source.getSerialNum(), e.eCode, e.description))
    except PhidgetException as e:
        print("Phidget Exception %i: %s" % (e.code, e.details))

def interfaceKitInputChanged(e):
    source = e.device
    print("InterfaceKit %i: Input %i: %s" % (source.getSerialNum(), e.index, e.state))

def interfaceKitSensorChanged(e):
    source = e.device
    print("InterfaceKit %i: Sensor %i: %i" % (source.getSerialNum(), e.index, e.value))

def interfaceKitOutputChanged(e):
    source = e.device
    print("InterfaceKit %i: Output %i: %s" % (source.getSerialNum(), e.index, e.state))

print("Opening phidget object....")

try:
    encoder.openPhidget()
    interfaceKit.openPhidget()
except PhidgetException as e:
    print("Phidget Error %i: %s" % (e.code, e.details))
    exit(1)


print("Waiting for attach....")

try:
    encoder.waitForAttach(10000)
except PhidgetException as e:
    print("Phidget Error %i: %s" % (e.code, e.details))
    try:
        encoder.closePhidget()
    except PhidgetException as e:
        print("Phidget Error %i: %s" % (e.code, e.details))
        exit(1)
    exit(1)
else:
    displayDeviceInfo()

print("Setting the data rate for each sensor index to 4ms....")

for i in range(interfaceKit.getSensorCount()):
    try:
        interfaceKit.setDataRate(i, 4)
    except PhidgetException as e:
        print("Phidget Exception %i: %s" % (e.code, e.details))



print "Codigo"

#Activar plot
plt.ion()
plt.figure(1) #presion
plt.grid(True)
plt.title("Lectura de presion en tiempo real")
plt.xlabel("Tiempo (s)")
plt.ylabel("Presion (Mpa)")

plt.figure(2) #angulo
plt.grid(True)
plt.title("Lectura del angulo en tiempo real")
plt.xlabel("Tiempo (s)")
plt.ylabel("Angulo (deg)")
y_presion = []
y_angulo = []

# Enable Encoder
encoder.setEnabled(3,True)
encoder.getEnabled(3)


itfk_anterior = interfaceKit.getSensorRawValue(0)
encoder_anterior = encoder.getPosition(3)

estado = encoder.getInputState(0)
print ("Input State :"), estado

while not (encoder.getInputState(0)):
    itfk_actual = interfaceKit.getSensorRawValue(0)
    encoder_actual = encoder.getPosition(3)

    volt = float(itfk_actual * 0.00122)
    presion = (((volt / 5.0) - 0.04) * 777.726) / 1000
    angle = float(encoder_anterior * (360.0 / 4800.0))

    # Add a table to the worksheet Angle.
    worksheet1.write(row, column_pos1, str(angle))
    worksheet1.write(row, column_pos2, str(time.clock()))
    # Add a table to the worksheet Pressure.
    worksheet2.write(row, column_pos1, str(presion))
    worksheet1.write(row, column_pos2, str(time.clock()))

    row += 1

    # plotting
    y_presion.append(presion)
    plt.figure(1)
    plt.plot(y_presion)
    plt.pause(0.01)
    y_angulo.append(angle)
    plt.figure(2)
    plt.plot(y_angulo)
    plt.pause(0.01)

    if (itfk_anterior != itfk_actual) or (encoder_anterior != encoder_actual):

        itfk_actual = interfaceKit.getSensorRawValue(0)

        volt = float(itfk_actual * 0.00122)
        presion = (((volt / 5.0) - 0.04) * 777.726)/1000

        #print "Valor Raw del voltaje leido en digital", itfk_actual
        print ("Voltaje leido en voltios %.2f") % volt
        #print ("Valor de la presion Raw %.2f") % presion
        print ("presion = %.2f") % presion

        itfk_anterior = itfk_actual
        encoder_anterior = encoder_actual
        angle = float(encoder_anterior*(360.0/4800.0))
        print ("angulo =" , float(angle))
        tiempo = time.clock()
        # Add a table to the worksheet Angle.
        worksheet1.write(row, column_pos1, str(angle))
        worksheet1.write(row, column_pos2, str(tiempo))
        # Add a table to the worksheet Pressure.
        worksheet2.write(row, column_pos1, str(presion))
        worksheet2.write(row, column_pos2, str(tiempo))

        row += 1

        #plotting
        y_presion.append(presion)
        plt.figure(1)
        plt.plot(y_presion)
        plt.pause(0.01)
        y_angulo.append(angle)
        plt.figure(2)
        plt.plot(y_angulo)
        plt.pause(0.01)

workbook.close()
print("Closing...")
try:
    encoder.closePhidget()
    interfaceKit.closePhidget()
except PhidgetException as e:
    print("Phidget Error %i: %s" % (e.code, e.details))
    print("Exiting....")
    exit(1)

print("Done.")
exit(0)